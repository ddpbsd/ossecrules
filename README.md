This project breaks the OSSEC rules and decoders out of the main tree.
The intention is to make them easier to work on, and possibly allow a user to upgrade the rules and decoders without upgrading OSSEC.


Configuration on the OSSEC server (ossec.conf):

```
<ossec_config>
  <rules>
    <decoder_dir pattern="_decoder.xml">etc/decoders.d</decoder_dir>
    ...
```
